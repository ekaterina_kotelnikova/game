package ru.kev.game;


/**
 * Класс реализует логику игры "Угадай число".
 *
 * @author Kotelnikova E.V. group15IT20
 */
public class GuessGame {

    public static final String YES = "Да!!! Это наш победитель!";
    public static final String NO = "К сожалению, нет :(";
    public static int targetNumber;

    /**
     * Метод позволяет определить игрока, который угадал загаданное число.
     */

    public void startGame() {
        Player firstPlayer = new Player();
        Player secondPlayer = new Player();
        Player thirdPlayer = new Player();

        System.out.println("Я думаю, что загаданное число равно любому от 0 до 9...");
        targetNumber = (int) (Math.random() * 10);

        while (true) {

            System.out.println("Загаданное число - " + targetNumber);

            boolean firstPlayerIsRight = playerGuess(firstPlayer);
            boolean secondPlayerIsRight = playerGuess(secondPlayer);
            boolean thirdPlayerIsRight = playerGuess(thirdPlayer);


            System.out.println("Первый игрок думает, что это число " + firstPlayer.getGuessedNumber());
            System.out.println("Второй игрок думает, что это число " + secondPlayer.getGuessedNumber());
            System.out.println("Третий игрок думает, что это число " + thirdPlayer.getGuessedNumber());

            if (firstPlayerIsRight || secondPlayerIsRight || thirdPlayerIsRight) {
                System.out.println("Кажется, у нас есть победитель! ;)");
                System.out.println("Первый игрок угадал? " + ((firstPlayerIsRight) ? YES : NO));
                System.out.println("Наверное, второй игрок угадал? " + ((secondPlayerIsRight) ? YES : NO));
                System.out.println("Может быть, третий игрок угадал? " + ((thirdPlayerIsRight) ? YES : NO));
                System.out.println("Игра закончена, не расстраивайтесь! Главное - не победа, а участие!");
                break;
            } else {
                System.out.println("Увы, победителя среди вас нет! Можете попробовать снова :с");
            }
        }
    }


    /**
     * Метод для угадывания игроком числа
     *
     * @param player ссылка на объект игрока
     * @return true, если игрок угадал число, иначе false
     */

    public boolean playerGuess(Player player) {
        player.guess();
        return player.getGuessedNumber() == targetNumber;
    }
}


