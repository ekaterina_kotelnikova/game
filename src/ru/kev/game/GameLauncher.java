package ru.kev.game;

/**
 * Класс для запуска игры.
 *
 * @author Kotelnikova E.V. group15IT20
 */

public class GameLauncher {
    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}