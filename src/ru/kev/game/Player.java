package ru.kev.game;

/**
 * Класс, описывающий игрока
 *
 * @author Kotelnikova E.V. group 15IT20
 */

public class Player {
    private int guessedNumber = 0;

    /**
     * Метод генерирует число для игрока.
     */
    public void guess() {
        guessedNumber = (int) (Math.random() * 10);
    }

    public int getGuessedNumber() {
        return guessedNumber;
    }
}
